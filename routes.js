const Joi = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mainUrl = process.env.BASE_URL;
var shortid = require('shortid');
const charPattern = /^https?:\/\/([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;

/* CREATING MONGOOSE SCHEMAS */

const shortnerSchema = new Schema({
    shortUrl: String,
    url: String,
    timeCreated: Date
});

const Shortner = mongoose.model('Shortner', shortnerSchema);

/* EXPORTING THE ROUTES */

module.exports = [
    {
        method: 'GET',
        path: '/',
        handler(request, reply) {
            reply.file('views/index.html');
        }
    }, {
        method: 'POST',
        path: '/shortener',
        handler(request, reply) {
            var shortCode = shortid.generate();
            const newShortener = new Shortner({
                shortUrl: `${mainUrl}/${shortCode}`,
                url: request.payload.url,
                timeCreated: new Date()
            });

            newShortener.save((err, redir) => {
                if (err) {
                    reply(err);
                } else {
                    reply(redir);
                }
            });
        },
        config: {
            validate: {
                payload: {
                    url: Joi.string().regex(charPattern).required()
                }
            }
        }
      }, {
        method: 'GET',
        path: '/{hash}',
        handler(request, reply) {
            const query = {
                'shortUrl': `${mainUrl}/${request.params.hash}`
            };

            Shortner.findOne(query, (err, redir) => {
                if (err) {
                    return reply(err);
                } else if (redir) {
                    reply().redirect(redir.url);
                } else {
                    reply.file('views/404.html').code(404);
                }
            });
        }
      },
    {
        method: 'GET',
        path: '/public/{file}',
        handler(request, reply) {
            reply.file('public/' + request.params.file);
        }
    }
];